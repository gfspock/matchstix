(function () {
    'use strict';
    var _countries = {
            'bangladesh': {
                name: 'BANGLADESH',
                galleryId: 140,
                uploadId: 141
            },
            'cambodia': {
                name: 'CAMBODIA',
                galleryId: 163,
                uploadId: 164
            },
            'fiji': {
                name: 'FIJI',
                galleryId: 165,
                uploadId: 166
            },
            'sri_lanka':{
                name:'SRI LANKA',
                galleryId:177,
                uploadId:178
            },
            'papua_new':{
                name:'PAPUA NEW',
                galleryId:179,
                uploadId:180
            }
        },
        _keys = Object.keys(_countries),
        _index = 'cambodia',
        _scriptId = 'shareroot',
        _element = '#gallery',
        _active = 'active',
        _hide = 'hide';

    var Switcher = function () {
    };
    Switcher.prototype.init = function () {
        this.update()
            ._events();
        return this;
    };

    Switcher.prototype.update = function () {

        this._removeScript()
            ._clearGallery()
            ._clearBtn()
            ._appendScript()
            ._updateFlag()
            ._updateCountry();
        return this;
    };

    Switcher.prototype._events = function () {
        var arrows = {};
        arrows.left = document.querySelector('.arrow.left');
        arrows.right = document.querySelector('.arrow.right');

        if (arrows.left && arrows.right) {
            arrows.left.addEventListener('click', this._prev.bind(this));
            arrows.right.addEventListener('click', this._next.bind(this));
        }
        return this;
    };

    Switcher.prototype._appendScript = function () {
        (function (url, conf) {
            var s = document.createElement("script");
            s.src = url;
            s.id = _scriptId;
            s.onload = function () {
                window._shareroot.config(conf);
            };
            (document.getElementsByTagName("head")[0] || document.body).appendChild(s);
        })("//sdk-sharerootinc.netdna-ssl.com/js/sdk.js", {
            upload: {id: _countries[_index].uploadId, hash: "upload"},
            gallery: {id: _countries[_index].galleryId, hash: "image", element: _element}
        });
        return this;
    };

    Switcher.prototype._removeScript = function () {
        var script = document.getElementById(_scriptId);
        if (script) {
            script.remove();
        }
        return this;
    };

    Switcher.prototype._clearGallery = function () {
        var gallery = document.querySelector(_element);
        if (gallery) {
            gallery.innerHTML = "";
        }
        return this;
    };

    Switcher.prototype._prev = function () {
        var i = _keys.indexOf(_index);
        i--;
        if (i < 0) {
            i = _keys.length - 1;
        }
        document.querySelector('.switcher').setAttribute('data-direction', 'left');
        this.setIndex(_keys[i]);
        return this;
    };
    Switcher.prototype._next = function () {
        var i = _keys.indexOf(_index);
        i++;
        if (i >= _keys.length) {
            i = 0;
        }
        document.querySelector('.switcher').setAttribute('data-direction', 'right');
        this.setIndex(_keys[i]);
        return this;
    };

    Switcher.prototype.setIndex = function (value) {
        console.log('setIndex', value);
        if (_index === value) {
            return this;
        }
        _index = value;
        this.update();
        return this;
    };

    Switcher.prototype._updateFlag = function () {
        var flags = document.querySelectorAll('.flags .flag');
        for (var i = 0; i < flags.length; i++) {
            if (flags[i].classList.contains(_active)) {
                flags[i].classList.remove(_active);
                flags[i].classList.add(_hide);
            } else {
                flags[i].classList.remove(_hide);
            }

            if (flags[i].getAttribute('data-country') === _index) {
                flags[i].classList.add(_active);
            }
        }
        return this;
    };
    Switcher.prototype._updateCountry = function () {
        var elems = document.querySelectorAll('.countries .country');
        for (var i = 0; i < elems.length; i++) {
            if (elems[i].classList.contains(_active)) {
                elems[i].classList.remove(_active);
                elems[i].classList.add(_hide);
            }else{
                elems[i].classList.remove(_hide);
            }
            if (elems[i].getAttribute('data-country') === _index) {
                elems[i].classList.add(_active);
            }
        }
        return this;
    };

    Switcher.prototype._clearBtn = function () {
        var html = '<a href="#upload" >ADD YOUR STORY</a>';
        var btn = document.getElementById('upload-btn');
        btn.innerHTML = '';
        btn.innerHTML = html;
        return this;
    };


    (new Switcher()).init();
})();
